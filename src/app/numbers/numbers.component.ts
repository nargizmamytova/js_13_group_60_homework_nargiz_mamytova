import { Component, Input, OnInit } from '@angular/core';
import { Roulette } from '../shared/roulette.modal';
import { RouletteService } from '../shared/roulette.service';

@Component({
  selector: 'app-numbers',
  templateUrl: './numbers.component.html',
  styleUrls: ['./numbers.component.css']
})
export class NumbersComponent implements OnInit{
  @Input() number!: Roulette;

  constructor(public rouletteService: RouletteService ) {

  }
  ngOnInit() {
    this.rouletteService.changeBalance(this.number.number);
  }

}
