import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { RouletteService } from './shared/roulette.service';
import { Roulette } from './shared/roulette.modal';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  @Input() inputNumber: number = 1;
  arrayNumber!: Roulette[];
  radioInput: any;
  constructor(public rouletteService: RouletteService) {
  }
  ngOnInit(){
    this.arrayNumber = this.rouletteService.getArrayNumber();
    this.rouletteService.newNumber.subscribe((numbers: Roulette[]) => {
      this.arrayNumber = numbers;
    });
  }

  onClickStart() {
   this.rouletteService.start();
    this.arrayNumber = this.rouletteService.getArrayNumber();
   }

  onClickStop() {
    this.rouletteService.stop();
  }

  onClickReset() {
    this.rouletteService.reset()
  }

  onChange() {
    this.radioInput = this.rouletteService.bid;
    this.inputNumber = this.rouletteService.bidNumber;
  }
}

