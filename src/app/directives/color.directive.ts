import { Directive, ElementRef, Input, Renderer2 } from '@angular/core';
import { RouletteService } from '../shared/roulette.service';

@Directive({
  selector: '[appColorClass]'
})
export class ColorDirective {
  colorClass = '';
  inputNumber = 0;
  @Input() set appColorClass(number: string){
    this.inputNumber = parseInt(number);
    this.addClass()
  }
  constructor(private el: ElementRef, private rouletteService: RouletteService, private renderer: Renderer2) {
  }
   addClass(){
    this.colorClass = this.rouletteService.getColor(this.inputNumber);
    this.renderer.addClass(this.el.nativeElement, this.colorClass)
  }

}
