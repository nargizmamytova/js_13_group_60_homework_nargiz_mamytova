import { Roulette } from './roulette.modal';
import { EventEmitter, Injectable, Output } from '@angular/core';

@Injectable()
export class RouletteService {
  @Output() newNumber = new EventEmitter();
  private arrayNumber: Roulette[] = [];
  balance: number = 100;
  bidNumber: number = 1;
  bid = 'red';
  intervalId: any ;
  getArrayNumber(){
    return this.arrayNumber.slice();
  }
  addNumberToArrayNumber(number: number){
      const numbers = new Roulette(number)
      this.arrayNumber.push(numbers);
    this.newNumber.emit(this.arrayNumber);
  }
  generateNumber(min: 0, max: 36){
    return  Math.floor(Math.random() * (max - min + 1)) + min;
  }
  interval() {
    this.intervalId = setInterval(() => {
      const numbers = this.generateNumber(0,36);
       this.addNumberToArrayNumber(numbers);
    }, 100 )
  }
  start(){
    if(!this.intervalId){
      this.interval();
    }
  }
  reset(){
    this.arrayNumber = [];
    this.newNumber.emit(this.arrayNumber);
  }
  stop(){
    clearInterval(this.intervalId)
  }
  getColor(number: number){
    if(number>= 1 && number <= 10 || number>= 19 && number<= 28){
      if(number % 2 === 0 ){
        return 'black';
      }else {
        return 'red'
      }
    } else if(number>= 11 &&  number<= 18 || number>= 29 && number<= 36){
      if(number % 2 === 0 ){
        return 'red';
      }else {
        return 'black';
      }
    }else if(number === 0){
      return 'zero'
    }else {
      return 'unknown'
    }
  }

  changeBalance(number: number){
    const result =  this.getColor(number)
    if(result === this.bid){
      if (this.bid === 'zero' ){
         this.balance = this.balance + 35;
      }else{
        this.balance += this.bidNumber;
      }
    }else if(result !== this.bid){
      this.balance = this.balance - this.bidNumber;
    }else {
      alert('Что-то пошло не так!!')
    }
  }
}
